##The Hindu - Archives

I have developed this assignment as a Rest Service using Spring Boot Framework.

I have added video recording of assignment proof. You can find it in root directory of repository.

**To execute**

To execute this project at your end you will be needing following:

1. Java 1.8
2. Browser
3. Before executing this project, make sure port 8767 is free and usable

Steps:

1. Download sent JAR achieve file (scraper-service-0.0.1-SNAPSHOT.jar)
2. Execute following command (make sure current directory is writable)

`java -jar /path/to/scraper-service-0.0.1-SNAPSHOT.jar`

**Verify output**

1. Once you hit that command - it will start scraping articles from 15/08/2009.
2. As it's REST Service - you need to open your browser

**API Endpoints**

1. Fetch all authors
`GET http://localhost:8767/authors`
2. Fetch specific author
`GET http://localhost:8767/authors/?name=[full name]&strict=yes`
3. Search for author
`GET http://localhost:8767/authors/?name=[search query]`
4. Fetch articles of a specific author
`GET http://localhost:8767/authors/[authorId]`
5. Fetch articles by author name
`GET http://localhost:8767/authors/articles/?name=[full name]&strict=yes`
6. Search articles by author name
`GET http://localhost:8767/authors/articles/?name=[search query]`
7. Fetch all articles
`GET http://localhost:8767/authors/articles`

**Database Access**

Open `http://localhost:8767/h2` in your browser. Use following details to get into the database console:

1. Driver class : `org.h2.Driver`
2. JDBC URL : `jdbc:h2:~/articles`
3. Username : `sa`
4. Password : ` ` <- it's empty

**Note** 

1. [authorId] will be in the response of first 3 API endpoints
2. Response format is JSON
3. Let me know if I can help you in anything
4. I haven't developed client for this REST Service but you can use popular PostMAN in Google Chrome