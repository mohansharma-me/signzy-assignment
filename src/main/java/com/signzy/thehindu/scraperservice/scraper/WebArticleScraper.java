package com.signzy.thehindu.scraperservice.scraper;

import com.signzy.thehindu.scraperservice.helpers.HashHelper;
import com.signzy.thehindu.scraperservice.models.ArticleMetadata;
import com.signzy.thehindu.scraperservice.models.Author;
import com.signzy.thehindu.scraperservice.repositories.ArticleMetadataRepository;
import com.signzy.thehindu.scraperservice.repositories.AuthorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class WebArticleScraper extends BaseScraper {

    private static final Logger log = LoggerFactory.getLogger(WebArticleScraper.class);

    private RestTemplate restTemplate;
    private ArticleMetadataRepository articleMetadataRepository;
    private AuthorRepository authorRepository;

    @Autowired
    public WebArticleScraper(RestTemplate restTemplate, ArticleMetadataRepository articleMetadataRepository, AuthorRepository authorRepository) {
        this.restTemplate = restTemplate;
        this.articleMetadataRepository = articleMetadataRepository;
        this.authorRepository = authorRepository;
    }

    private LocalDate getStartDate() {
        return LocalDate.parse("2009-08-15");
    }

    private ArrayList<String> extractArticleLinks(String htmlContent) {
        ArrayList<String> articleLinks = new ArrayList<>();

        Pattern p = Pattern.compile("href=\"https://www\\.thehindu\\.com/(.*?)\\.ece\"");
        Matcher m = p.matcher(htmlContent);
        while(m.find()) {
            articleLinks.add(
                    m.group()
                    .replace("href=\"","")
                    .replace("\"","")
            );
        }

        return articleLinks;
    }

    private ArticleMetadata extractArticleMetadata(String articleHTMLContent) {
        if(!StringUtils.isEmpty(articleHTMLContent)) {
            String articleTitle = parseArticleTitle(articleHTMLContent);
            String articleDescription = parseArticleDescription(articleHTMLContent);
            String authorName = parseArticleAuthorName(articleHTMLContent);
            String articleCreatedOn = parseCreatedDate(articleHTMLContent);
            String[] tags = parseArticleTags(articleHTMLContent);

            if(StringUtils.isEmpty(authorName)) {
                authorName = "Unknown";
            }

            if(!StringUtils.isEmpty(articleTitle) && !StringUtils.isEmpty(articleDescription) && !StringUtils.isEmpty(authorName) && !StringUtils.isEmpty(articleCreatedOn)) {
                ArticleMetadata articleMetadata = new ArticleMetadata();
                articleMetadata.setTitle(articleTitle);
                articleMetadata.setDescription(articleDescription);
                articleMetadata.setAuthorId(HashHelper.getHash(authorName));
                articleMetadata.setAuthorName(authorName);
                articleMetadata.setTags(tags);
                articleMetadata.setCreatedDate(articleCreatedOn);
                return articleMetadata;
            }
        }

        return null;
    }

    private int processArticleLinks(ArrayList<String> articleLinks) {
        if(articleLinks == null) return 0;
        if(articleLinks.size() < 1) return 0;

        log.info("Started processing articles!");

        int successCount = 0;
        for(String articleLink : articleLinks) {
            String articleHTMLContent = fetchHTMLContent(
                    restTemplate,
                    articleLink
            );

            if(!StringUtils.isEmpty(articleHTMLContent)) {
                ArticleMetadata articleMetadata = extractArticleMetadata(articleHTMLContent);
                if(articleMetadata != null) {
                    articleMetadata.setLink(articleLink);

                    try {
                        articleMetadata = articleMetadataRepository.save(articleMetadata);
                        log.info("Article created! [{}]", articleMetadata);
                    } catch (Exception e) {
                        log.info("Error in storing article! Error : {}", e.getMessage());
                        e.printStackTrace();
                    }

                    try {
                        if(!authorRepository.existsById(articleMetadata.getAuthorId())) {
                            Author author = new Author();
                            author.setId(articleMetadata.getAuthorId());
                            author.setName(articleMetadata.getAuthorName());
                            author = authorRepository.save(author);
                            log.info("Author created! [{}]", author);
                        }
                    } catch (Exception e) {
                        log.info("Error in finalizing author! Error : {}", e.getMessage());
                        e.printStackTrace();
                    }

                    successCount++;
                }
            }
        }

        log.info("#{} articles stored successfully out of {}", successCount, articleLinks.size());

        return successCount;
    }

    public void start() {
        LocalDate startDate = getStartDate();

        log.info("Starting web article scraping from date : {}", startDate);

        for (LocalDate date = startDate; date.isBefore(LocalDate.now()); date = date.plusDays(1)) {

            log.info("Reading articles of date = {}", date);

            String htmlContent = fetchHTMLContent(
                    restTemplate,
                    getWebArticlesURIOf(date.getYear(), date.getMonthValue(), date.getDayOfMonth())
            );

            if (!StringUtils.isEmpty(htmlContent)) {
                ArrayList<String> articleLinks = extractArticleLinks(htmlContent);
                log.info("#"+articleLinks.size()+" articles found!");
                if(articleLinks.size() > 0) {
                    processArticleLinks(articleLinks);
                }
            } else {
                log.info("Unable to fetch HTML content!");
            }
        }
    }

}
