package com.signzy.thehindu.scraperservice.scraper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class BaseScraper {

    private static final Logger log = LoggerFactory.getLogger(BaseScraper.class);

    public final String getWebArticlesURIOf(int year, int month, int day) {
        return "https://www.thehindu.com/archive/web/{year}/{month}/{day}/"
                .replace("{year}", year+"")
                .replace("{month}", month+"")
                .replace("{day}", day+"");
    }

    public final String fetchHTMLContent(RestTemplate restTemplate, String urlOfContent) {
        String htmlContent = null;
        try {
            log.info("Fetching content from {}", urlOfContent);
            ResponseEntity<String> responseEntity = restTemplate.exchange(
                    urlOfContent,
                    HttpMethod.GET,
                    null,
                    String.class
            );

            if(responseEntity.getStatusCode().is2xxSuccessful()) {
                htmlContent = responseEntity.getBody();
            } else {
                log.info("Response error : status code is "+responseEntity.getStatusCodeValue());
            }
        } catch (Exception e) {
            log.info("Error in fetching HTML content of URL : {}. (Exception: {})", urlOfContent, e.getMessage());
            e.printStackTrace();
        }

        return htmlContent;
    }

    public final String parseArticleTitle(String htmlContent) {
        if(!StringUtils.isEmpty(htmlContent)) {
            Pattern titlePattern = Pattern.compile("<meta name=\"title\" content=\"(.*?)\"/>");
            Matcher titleMatcher = titlePattern.matcher(htmlContent);
            if(titleMatcher.find()) {
                return titleMatcher.group(1);
            }
        }
        return null;
    }

    public final String parseArticleDescription(String htmlContent) {
        if(!StringUtils.isEmpty(htmlContent)) {
            Pattern pattern = Pattern.compile("<meta name=\"description\" content=\"(.*?)\"/>");
            Matcher matcher = pattern.matcher(htmlContent);
            if(matcher.find()) {
                return matcher.group(1);
            }
        }
        return null;
    }

    public final String parseArticleAuthorName(String htmlContent) {
        if(!StringUtils.isEmpty(htmlContent)) {
            Pattern pattern = Pattern.compile("<meta property=\"article:author\" content=\"(.*?)\"/>");
            Matcher matcher = pattern.matcher(htmlContent);
            if(matcher.find()) {
                return matcher.group(1);
            }
        }
        return null;
    }

    public final String[] parseArticleTags(String htmlContent) {
        if(!StringUtils.isEmpty(htmlContent)) {
            Pattern pattern = Pattern.compile("<meta property=\"article:tag\" content=\"(.*?)\"/>");
            Matcher matcher = pattern.matcher(htmlContent);
            ArrayList<String> tagList = new ArrayList<>();
            while(matcher.find()) {
                tagList.add(matcher.group(1));
            }
            return tagList.toArray(new String[tagList.size()]);
        }
        return null;
    }

    public final String parseCreatedDate(String htmlContent) {
        if(!StringUtils.isEmpty(htmlContent)) {
            Pattern pattern = Pattern.compile("<meta name=\"created-date\" content=\"(.*?)\"/>");
            Matcher matcher = pattern.matcher(htmlContent);
            if(matcher.find()) {
                return matcher.group(1);
            }
        }
        return null;
    }

}
