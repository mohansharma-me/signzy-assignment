package com.signzy.thehindu.scraperservice.services;

import com.signzy.thehindu.scraperservice.models.ArticleMetadata;
import com.signzy.thehindu.scraperservice.models.Author;
import com.signzy.thehindu.scraperservice.repositories.ArticleMetadataRepository;
import com.signzy.thehindu.scraperservice.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;

@Service
public class SearchService {

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    ArticleMetadataRepository articleMetadataRepository;

    public ArrayList<Author> searchAuthorByName(String name) {
        if(!StringUtils.isEmpty(name)) {
            return (ArrayList<Author>) authorRepository.findByNameContaining(name);
        }
        return new ArrayList<>();
    }

    public ArrayList<Author> getAuthorByName(String name) {
        if(!StringUtils.isEmpty(name)) {
            return (ArrayList<Author>) authorRepository.findByName(name);
        }
        return new ArrayList<>();
    }

    public ArrayList<Author> getAllAuthors() {
        return (ArrayList<Author>) authorRepository.findAll();
    }

    public ArrayList<ArticleMetadata> getArticlesByAuthorId(String authorId) {
        if(!StringUtils.isEmpty(authorId)) {
            return (ArrayList<ArticleMetadata>) articleMetadataRepository.findByAuthorId(authorId);
        }
        return new ArrayList<>();
    }

    public ArrayList<ArticleMetadata> getArticlesByName(String authorName) {
        if(!StringUtils.isEmpty(authorName)) {
            return (ArrayList<ArticleMetadata>) articleMetadataRepository.findByAuthorName(authorName);
        }
        return new ArrayList<>();
    }

    public ArrayList<ArticleMetadata> searchArticlesByName(String authorName) {
        if(!StringUtils.isEmpty(authorName)) {
            return (ArrayList<ArticleMetadata>) articleMetadataRepository.findByAuthorNameContaining(authorName);
        }
        return new ArrayList<>();
    }

    public ArrayList<ArticleMetadata> getAllArticles() {
        return (ArrayList<ArticleMetadata>) articleMetadataRepository.findAll();
    }
}
