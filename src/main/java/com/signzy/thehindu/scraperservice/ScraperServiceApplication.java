package com.signzy.thehindu.scraperservice;

import com.signzy.thehindu.scraperservice.scraper.WebArticleScraper;
import org.apache.catalina.core.ApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ScraperServiceApplication {

	public static void main(String[] args) {
		final ConfigurableApplicationContext applicationContext = SpringApplication.run(
				ScraperServiceApplication.class,
				args
		);

		new Thread(() -> {
			// Start web article scraper
			WebArticleScraper webArticleScraper = applicationContext.getBean(WebArticleScraper.class);
			webArticleScraper.start();
		}).start();
	}

}

