package com.signzy.thehindu.scraperservice.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(indexes = { @Index(name = "name_index", columnList = "name")})
public class Author {
    @Id
    private String id;

    private String name;
}
