package com.signzy.thehindu.scraperservice.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(indexes = { @Index(name = "author_index", columnList = "authorId"), @Index(name = "title_index", columnList = "title")})
@Data
public class ArticleMetadata {
    @Id
    @GeneratedValue
    private Long id;

    private String link;
    private String title;
    @Column(length = 1024)
    private String description;
    private String[] tags;
    private String authorId;
    private String authorName;
    private String createdDate;
}
