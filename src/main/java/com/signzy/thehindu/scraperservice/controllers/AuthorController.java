package com.signzy.thehindu.scraperservice.controllers;

import com.signzy.thehindu.scraperservice.models.ArticleMetadata;
import com.signzy.thehindu.scraperservice.models.Author;
import com.signzy.thehindu.scraperservice.services.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/authors")
public class AuthorController {

    @Autowired
    SearchService searchService;

    @GetMapping
    public ResponseEntity<List<Author>> handleGetAuthors(
            @RequestParam("name") Optional<String> searchQuery,
            @RequestParam("strict") Optional<String> strictSearch
    ) {
        ArrayList<Author> resultedAuthors = null;

        if(searchQuery.isPresent()) {
            if(strictSearch.isPresent()) {
                resultedAuthors = searchService.getAuthorByName(searchQuery.get());
            } else {
                resultedAuthors = searchService.searchAuthorByName(searchQuery.get());
            }
        } else {
            resultedAuthors = searchService.getAllAuthors();
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(resultedAuthors);
    }

    @GetMapping("/{authorId}")
    public ResponseEntity<List<ArticleMetadata>> handleGetArticles(
            @PathVariable("authorId") String authorId
    ) {
        if(!StringUtils.isEmpty(authorId)) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(
                            searchService.getArticlesByAuthorId(authorId)
                    );
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .build();
        }
    }

    @GetMapping("/articles")
    public ResponseEntity<List<ArticleMetadata>> handleSearchArticlesByName(
            @RequestParam("name") Optional<String> searchQuery,
            @RequestParam("strict") Optional<String> strictSearch
    ) {
        ArrayList<ArticleMetadata> resultedArticles = null;

        if(searchQuery.isPresent()) {
            if(strictSearch.isPresent()) {
                resultedArticles = searchService.getArticlesByName(searchQuery.get());
            } else {
                resultedArticles = searchService.searchArticlesByName(searchQuery.get());
            }
        } else {
            resultedArticles = searchService.getAllArticles();
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(resultedArticles);
    }

}
