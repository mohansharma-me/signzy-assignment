package com.signzy.thehindu.scraperservice.repositories;

import com.signzy.thehindu.scraperservice.models.Author;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends CrudRepository<Author, String> {
    Iterable<Author> findByName(String name);
    Iterable<Author> findByNameContaining(String name);
}
