package com.signzy.thehindu.scraperservice.repositories;

import com.signzy.thehindu.scraperservice.models.ArticleMetadata;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleMetadataRepository extends CrudRepository<ArticleMetadata, Long> {
    Iterable<ArticleMetadata> findByAuthorId(String authorId);
    Iterable<ArticleMetadata> findByAuthorName(String authorName);
    Iterable<ArticleMetadata> findByAuthorNameContaining(String authorName);
}
