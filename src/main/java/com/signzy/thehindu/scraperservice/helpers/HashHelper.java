package com.signzy.thehindu.scraperservice.helpers;

import org.springframework.util.DigestUtils;

public class HashHelper {
    public static String getHash(String data) {
        return DigestUtils.md5DigestAsHex(data.getBytes());
    }
}
